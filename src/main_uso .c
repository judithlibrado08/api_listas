/*
* Este programa sirve para verificar el funcionamiento del API "arreglo_lista.so"
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "arreglo_lista.h"

int main(int argc, char const *argv[]){
	
	classLista *obj = new_ObjLista();
	classLista *ob2 = new_ObjLista();
	assert(obj != NULL && ob2 != NULL);

	obj->setElementosListaInt(obj, 5);
	printf("\n\n");
	obj->printListaInt(obj);
	printf("\n");
	
	obj->setValorListaInt(obj, 0, 1);
	obj->setValorListaInt(obj, 1, 2);
	obj->setValorListaInt(obj, 2, 3);
	obj->setValorListaInt(obj, 3, 4);
	obj->setValorListaInt(obj, 4, 5);

	printf("Arreglo....\n");
	obj->printListaInt(obj);
	printf("\n");

	ob2->setElementosListaFloat(ob2, 10);
	printf("\n");
	ob2->printListaFloat(ob2);
	printf("\n");
	
	ob2->setValorListaFloat(ob2, 0, 1.54343);
	ob2->setValorListaFloat(ob2, 1, 2.74343);
	ob2->setValorListaFloat(ob2, 2, 3.83434);
	ob2->setValorListaFloat(ob2, 3, 4.84343);
	ob2->setValorListaFloat(ob2, 4, 5.83434);
	ob2->setValorListaFloat(ob2, 5, 6.83434);
	ob2->setValorListaFloat(ob2, 6, 7.83434);
	ob2->setValorListaFloat(ob2, 7, 8.83434);
	ob2->setValorListaFloat(ob2, 8, 9.83434);
	ob2->setValorListaFloat(ob2, 9, 10.13434);
	printf("Arreglo despues de meter valores....\n");
	ob2->printListaFloat(ob2);

	free_ObjLista(obj);
	free_ObjLista(ob2);
	printf("\tBien\n\n");
	return 0;
}
