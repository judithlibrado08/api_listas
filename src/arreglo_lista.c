 /* ****************************************************************************
  * Inclusión de las definiciones del modulo.
  * ***************************************************************************/
 #include "arreglo_lista.h"
#include <stdio.h>
#include <stdlib.h>
 
 /*********************************************************************
 **                                                                  **
 ** PROTOTIPOS DE FUNCIONES LOCALES                                  **
 **                                                                  **
 *********************************************************************/

 // ----> Para un array unidimensional de tipo float
 // Define el número de elementos para un array bidi float
 static void set_ElementosListaFloat(classLista *this, int elementos);
 // Se obtiene un array uni de tipo float
 static float *get_ListaFloat(classLista *this);
 // Se define de una sola vez un array uni de tipo float (array_uni es de entrada)
 static void set_ListaFloat(classLista *this, float *array_uni, int num_elementos);
 // Se agrega un valor float al array uni de tipo float
 static void set_ValorListaFloat(classLista *this, int indice, float valor);
 // Se obtiene un valor float del array uni de tipo float 
 static float get_ValorListaFloat(classLista *this, int indice);
 // Se libera la memoria de un array uni de tipo float   
 static void free_ListaFloat(classLista *this);
 // Imprimir el array uni de tipo float
 static void print_ListaFloat(classLista *this);
 // Obtener el numero de elementos del array uni float
 static int get_TotalElementosListaFloat(classLista *this);
 
 // ----> Para un array unidimensional de tipo int
 // Define el número de elementos para un array bidi int
 static void set_ElementosListaInt(classLista *this, int elementos);
 // Se obtiene un array uni de tipo int
 static int *get_ListaInt(classLista *this);
 // Se define de una sola vez un array uni de tipo int (array_uni es de entrada)
 static void set_ListaInt(classLista *this, int *array_uni, int num_elementos);
 // Se agrega un valor int al array uni de tipo int
 static void set_ValorListaInt(classLista *this, int indice, int valor);
 // Se obtiene un valor int del array uni de tipo int 
 static int get_ValorListaInt(classLista *this, int indice);
 // Se libera la memoria de un array uni de tipo int     
 static void free_ListaInt(classLista *this);
 // Imprimir el array uni de tipo int
 static void print_ListaInt(classLista *this);
 // Obtener el numero de elementos del array uni int
 static int get_TotalElementosListaInt(classLista *this);

 static int validarElementos(int elementos);
 
 static void inicializarListaFloat(classLista *this);
 static void inicializarListaInt(classLista *this);
 
 struct StructNodo{
 	int dato;
	
 };
 
 struct CLASS_ATT_LISTA{
     float *array_uni_float; // array unidimensional de tipo float
     int *array_uni_int; // array unidemensional de tipo int
     int elementos_float; // elementos para un array unidimensional float
     int elementos_int; // elementos para un array unidemensional int    
 };
 


/*********************************************************************
 **                                                                  **
 ** LISTA ENLAZADAS CON PILA                                         **
 **                                                                  **
 **********************************************************************/
/*struct structNodo2{
	int dato;
	struct structNodo2*siguiente;
};

typedef struct structNodo2 nodo2;
nodo2*crearPila(nodo2*pila);
int pilaVacia(nodo2*pila);
nodo2*topePila(nodo2*pila);
nodo2*push(int valor,nodo2*pila);
nodo2*pop(nodo2*pila);


//Crear pila
nodo2*crearPila(nodo2*pila){
	return pila=NULL;
}

int pilaVacia(nodo2 *pila){
	if(pila==NULL)
		return 1;

	return 0;
}

nodo2*topePila(nodo2*pila){
	return pila;

}

nodo2*push(int valor,nodo2*pila){
	nodo2*nodoNuevo;
	nodoNuevo= (nodo2 *) malloc (sizeof (nodo2));

	if(nodoNuevo !=NULL){
		nodoNuevo->dato=valor;
		nodoNuevo->siguiente=pila;
		pila=nodoNuevo;

}
	return pila;
}

nodo2*pop(nodo2 *pila){
	nodo2 *nodoAuxiliar;
	if(!pilaVacia(pila)){
		nodoAuxiliar=pila;
		pila=pila->siguiente;
		free(nodoAuxiliar);

}
	return pila;
}*/


/*********************************************************************
 **                                                                  **
 ** CONVERSIONES DE EXPRESIONES INFIJAS A POSTFIJAS                  **
 **                                                                  **
 **********************************************************************/
/*struct structNodo3{
	char simbolo;
	struct structNodo3*siguiente;
}

typedef struct structNodo3 nodo3;
nodo3* crearLista(nodo3 *lista);
nodo3*push(char simbolo,nodo3*pila);
nodo3 *insertNodoFinal(char simbolo,nodo3*lista);
nodo3*pop(char *valor,nodo3*pila);

int expresionBalanceada(char expresion []);
int esOperador(char simbolo);
int prioridad (char operador);
nodo3 *infijaPostfija(char expresion []);


nodo3* crearLista(nodo3 *lista){
	return lista=NULL;
}

nodo3 *push(char simbolo,nodo3 *pila){
	nodo3 *nodoNuevo;
	nodoNuevo =(nodo3 *) malloc (sizeof (nodo3));

	if(nodoNuevo !=NULL){
		nododNuevo ->simbolo=simbolo;
		nodoNuevo ->siguiente=pila;

		pila=nodoNuevo;
}
	return pila;
}

//INsertar nodo 

nodo3 *insertNodoFinal(char simbolo,nodo3*lista){
	nodo3 *nodoNuevo,*nodoAuxiliar;
	nodoNuevo= (nodo3 *) malloc (sizeof (nodo3));

	if(nodoNuevo !=NULL){
		nododNuevo ->simbolo=simbolo;
		nodoNuevo ->siguiente=NULL;
		
		if(lista=NULL) lista=nodoNuevo;
		else{
			nodoAuxiliar=lista;
			while(nodoAuxiliar ->siguiente!=NULL)
				nodoAuxiliar=nodoAuxiliar ->siguiente;
			nodoAuxilixar ->siguiente=nodoAuxiliar;
}
}
	return pila;
}


nodo3 *pop(char *valor,nodo3 *pila){
	nodo3 *nodoAuxiliar;
	char dato;

	if(pila==NULL)
	prntf ("Pila Vacia");
	else {
		nodoAuxiliar=pila;
		dato=nodoAuxiliar->simbolo;
		pila=nodoAuxiliar->siguiente;
		*valor=dato;
		free(nodoAuxilixar);
}
	return pila;
}


//Expresiones balanceadas
int expresionBalanceada (char expresion []){
	nodo *pilaAuxiliar;
	int longitudExpresion, i;
	char valor;

	pilaAuxiliar=crearLista(pilaAuxiliar);
	longitudExpresion=strien(expresion);

	for(i=0;i<longitudExpresion;i++){
		if (expresion [i]== '(')
			pilaAuxiliar=push(expresion [i], pilaAuxiliar);

		if(expresion [i]== ')')
			pilaAuxiliar=pop(&valor,pilaAuxiliar);
}
	if(pilaAuxiliar==NULL)
		return 1;
	return 0;
}



int esOPerador (char simbolo){
	if ((simbolo=='+') || (simbolo =='-') || (simbolo =='*') || (simbolo== '/') || (simbolo =='^'))
		return 1;
	return 0;
}


int prioridad(char operador){
	if(operador=='^')
		return 3;

	if((operador=='*') || (operador=='/'))		
		return 2;

	if((operador=='+') || (operador=='-'))		
		return 1;

	if(operador=='(')
		return 0;

}

//Conversiòn
nodo*infijaPostfija(char expresion []){
	nodo *pila, *postFija;
	int longitudExpresion,i;
	char valor;

	pila=crearLista(pila);
	postFija=crearLista(postFija);
	longitudExpresion=strien(expresion);

	for(i=0;i<longitudExpresion;i++){
		if(((expresion [i]>=48) && (expresion [i]<=57)) || ((expresion [i]>=65) && (expresion [i]<=90)) || ((expresion[i]>=97 )&& (expresion[i]<=122)))
			postFija=insertNodoFinal(expresion[i],postFija);

		else{ 
			if(expresion [i]=='(')
				pila=push(expresion[i],pila);
			else{
				if((esOperador(expresion [i]))){
					if(pila==NULL)
						pila=push(expresion[i],pila);
					else{
						while (pila!=NULL){
							if((prioridad(pila->simbolo)>=prioridad(expresion[i]))){
							pila=pop(&valor,pila);
							postFija=insertNodoFinal(valor,postFija);
			

	}
							else
								break;

	}
						pila=push(expresion[i],pila);
}

}

}
}
	if(expresion[i]==')'){
		while((pila->simbolo!='(') && (pila !=NULL)){
			pila=pop(&valor,pila);
			postFija=insertNodoFinal(valor,postFija);

}
			pila=pop(&valor,pila);
}

}
	while(pila !=NULL){
		pila=pop(&valor,pila);
		postFija=insertNodoFinal(valor,postFija);
}
	return postFija;
}
*/

 /********************************************************************
 **                                                                  **
 ** IMPLEMENTACION DE FUNCIONES PUBLICAS                             **
 **                                                                  **
 **********************************************************************/
 
 // Método para crear un objeto de tipo classLista
 classLista *new_ObjLista(){
     classLista *this = (classLista *) malloc(sizeof(classLista));
     if(this != NULL){
         this->atributos = (classAttLista *) malloc(sizeof(classAttLista));
         if(this->atributos != NULL){
             this->setElementosListaFloat = set_ElementosListaFloat; 
             this->getListaFloat = get_ListaFloat;
             this->setListaFloat = set_ListaFloat;
             this->setValorListaFloat = set_ValorListaFloat;
             this->getValorListaFloat = get_ValorListaFloat;
             this->freeListaFloat = free_ListaFloat;
             this->printListaFloat = print_ListaFloat;
             this->getTotalElementosListaFloat = get_TotalElementosListaFloat;
	     this->siguiente=NULL;
 
             this->setElementosListaInt = set_ElementosListaInt; 
             this->getListaInt = get_ListaInt;
             this->setListaInt = set_ListaInt;
             this->setValorListaInt = set_ValorListaInt;
             this->getValorListaInt = get_ValorListaInt; 
             this->freeListaInt = free_ListaInt;
             this->printListaInt = print_ListaInt;
             this->getTotalElementosListaInt = get_TotalElementosListaInt;
 
             this->atributos->array_uni_float = NULL; // array unidimensional de tipo float
             this->atributos->elementos_float = 1; // elementos para un array unidimensional float
	     
             this->atributos->array_uni_int = NULL; // array unidemensional de tipo int
             this->atributos->elementos_int = 1; // elementos para un array unidemensional int   
         }
         else{
             printf("No se pudo crear memoria para this->atributos\n");
         }
     }
     else{
         printf("No se pudo crear memoria para this.\n");
     }
     return this;
 }
 
 // Método para eliminar un objeto de tipo classLista
 void free_ObjLista(classLista *this){
     if(this != NULL){
         if(this->atributos != NULL){
             free(this->atributos);
             free(this);
             printf("\n\tSe libero de manera correcta la memoria del objeto this...\n");
         }
         else{
             printf("this->atributos == NULL, no hay nada que liberar...\n");    
         }
     }
     else{
         printf("this == NULL, no hay nada que liberar...\n");
     }
 }
 
 /*********************************************************************
 **                                                                  **
 ** IMPLEMENTACION DE FUNCIONES PRIVADAS                             **
 **                                                                  **
 **********************************************************************/
 // Uso local
  
 static int validarElementos(int elementos){
     int correcto = 0;
     if(elementos > 0 && elementos < TAM_ELEMENTOS){
         correcto = 1;
     }
     return correcto;
 }
 
 static void inicializarListaFloat(classLista *this){
     int i;
     for(i = 0; i < this->atributos->elementos_float; i++){
         this->atributos->array_uni_float[i] = 0.0;
     }
 }
 
 static void inicializarListaInt(classLista *this){
     int i;
     for(i = 0; i < this->atributos->elementos_int; i++){
         this->atributos->array_uni_int[i] = 0;
     }
 }

 // ----> Para un array unidimensional de tipo float
 // Define el número de elementos para un array bidi float
 static void set_ElementosListaFloat(classLista *this, int elementos){
     if(this != NULL){
         if(validarElementos(elementos) != 0){
             if(this->atributos->array_uni_float != NULL){
                 printf("El arreglo unidimensional ya tiene memoria asignada,\nse eliminará y se creará nueva.\n");              
                 free_ListaFloat(this);
             }
             this->atributos->elementos_float = elementos;
             this->atributos->array_uni_float = (float *)malloc(this->atributos->elementos_float*sizeof(float));
             if(this->atributos->array_uni_float != NULL){
                 printf("Creación de memoria correcta para número de elementos <<%d>>\n", this->atributos->elementos_float);
                 inicializarListaFloat(this);                
             }
             else{
                 printf("Error en la creación de memoria para el total de elementos <<%d>>...\n", this->atributos->elementos_float);
             }
         }
         else{
           printf("Error --> El número de elementos <<%d>> debe ser mayor a 0 y menor que %d.\n", elementos, TAM_ELEMENTOS);
         }
     }
     else{
       printf("El objeto this no tiene memoria asignada...\n");
     }
 }
 
 // Se obtiene un array uni de tipo float
 static float *get_ListaFloat(classLista *this){
     float *array_uni = NULL;
     int i;
     int error = 0;
     if(this != NULL){
         if(this->atributos->array_uni_float != NULL){
             array_uni = (float *)malloc(this->atributos->elementos_float*sizeof(float));
             if(array_uni == NULL){
                 error = 1;
                 printf("Error en la creación de memoria para el total de elementos <<%d>>...\n", this->atributos->elementos_float);
             }
             if(error == 0){
                 // Copiar valores de this->atributos->array_uni_float a array_uni
                 for(i = 0; i < this->atributos->elementos_float; i++){
                     array_uni[i] = this->atributos->array_uni_float[i];
                 }
             }           
         }
         else{
             printf("El array uni float no tiene memoria asignada...\n");
         }
     }
     else{
         printf("El objeto this no tiene memoria asignada...\n");
     }
     return array_uni;
 }
 
 // Se define de una sola vez un array uni de tipo float (array_uni es de entrada)
 static void set_ListaFloat(classLista *this, float *array_uni, int num_elementos){  
     if(this != NULL && array_uni != NULL){
       if(num_elementos >= 0){
         set_ElementosListaFloat(this, num_elementos);       
         int i;
         for(i = 0; i < get_TotalElementosListaFloat(this); i++){
         this->atributos->array_uni_float[i] = array_uni[i];
         }    
       }
       else{
         printf("\n--> num_elementos <<%d>> debe ser mayor o igual a 0...\n", num_elementos);
       }
     }
     else{
         printf("El objeto this no tiene memoria asignada ó array_bidi == NULL...\n");
     }
 }
 
 // Se agrega un valor float al array uni de tipo float
 static void set_ValorListaFloat(classLista *this, int indice, float valor){
     if(this != NULL){
         if(this->atributos->array_uni_float != NULL){
             if((indice >= 0 && indice < this->atributos->elementos_float)){
                 this->atributos->array_uni_float[indice] = valor;
             }
             else{             
               printf("Error en el indice --> <<%d>>\n", indice);
             }
         }
         else{ 
           printf("El array uni float no tiene memoria asignada...\n");
         }
     }
     else{
       printf("El objeto this no tiene memoria asignada...\n");
     }
 }
 
 // Se obtiene un valor float del array uni de tipo float 
 static float get_ValorListaFloat(classLista *this, int indice){
     float valor = -1.0;
     if(this != NULL){
         if(this->atributos->array_uni_float != NULL){
             if((indice >= 0 && indice < this->atributos->elementos_float)){
                 valor = this->atributos->array_uni_float[indice];
             }
             else{
                 printf("Error en el indice --> <<%d>>, se retorna -1.0...\n", indice);
             }
         }
         else{ 
             printf("El array uni float no tiene memoria asignada...\n");
         }
     }
     else{
         printf("El objeto this no tiene memoria asignada...\n");
     }
     return valor;
 }
 
 // Se libera la memoria de un array uni de tipo float   
 static void free_ListaFloat(classLista *this){
     if(this != NULL){
         if(this->atributos->array_uni_float != NULL){ // Tiene memoria ya asignada  
             free(this->atributos->array_uni_float);
             this->atributos->array_uni_float = NULL;
             this->atributos->elementos_float = 1;
             printf("\n\tSe libero de manera correcta la memoria del array_uni_float...\n");
         }
         else{
             printf("El array uni float no tiene memoria asignada...\n");
         }
     }
     else{
         printf("El objeto this no tiene memoria asignada...\n");
     }
 }
 
 // Imprimir el array uni de tipo float
 static void print_ListaFloat(classLista *this){
     if(this != NULL){
         if(this->atributos->array_uni_float != NULL){
             int i;
             for(i = 0; i < this->atributos->elementos_float; i++){
                 printf(" %f ", this->atributos->array_uni_float[i]);
             }
             printf("\n");
         }
         else{
             printf("El array uni float no tiene memoria asignada...\n");
         }
     }
     else{
         printf("El objeto this no tiene memoria asignada...\n");
     }
 }
 
 // Obtener el numero de elementos del array uni float
 static int get_TotalElementosListaFloat(classLista *this){
     int elementos = -1;
     if(this != NULL){
         if(this->atributos->array_uni_float != NULL){
             elementos = this->atributos->elementos_float;
         }
         else{
             printf("this->atributos->array_uni_float no tiene memoria asignada...\n");
         }
     }
     else{
         printf("El objeto this no tiene memoria asignada...\n");
     }
     return elementos;
 }
 // ----> Para un array unidimensional de tipo int
 // Define el número de elementos para un array bidi int
 static void set_ElementosListaInt(classLista *this, int elementos){
     if(this != NULL){
         if(validarElementos(elementos) != 0){
             if(this->atributos->array_uni_int != NULL){
                 printf("El arreglo unidimensional ya tiene memoria asignada,\nse eliminará y se creará nueva.\n");              
                 free_ListaInt(this);
             }
             this->atributos->elementos_int = elementos;
             this->atributos->array_uni_int = (int *)malloc(this->atributos->elementos_int*sizeof(int));
             if(this->atributos->array_uni_int != NULL){
                 printf("Creación de memoria correcta para número de elementos <<%d>>\n", this->atributos->elementos_int);
                 inicializarListaInt(this);              
             }
             else{
                 printf("Error en la creación de memoria para el total de elementos <<%d>>...\n", this->atributos->elementos_int);
             }
         }
         else{
             printf("Error --> El número de elementos <<%d>> debe ser mayor a 0 y menor que %d.\n", elementos, TAM_ELEMENTOS);
         }
     }
     else{
         printf("El objeto this no tiene memoria asignada...\n");
     }   
 }
 
 // Se obtiene un array uni de tipo int
 static int *get_ListaInt(classLista *this){
     int *array_uni = NULL;
     int i;
     int error = 0;
     if(this != NULL){
         if(this->atributos->array_uni_int != NULL){
             array_uni = (int *)malloc(this->atributos->elementos_int*sizeof(int));
             if(array_uni == NULL){
                 error = 1;
                 printf("Error en la creación de memoria para el total de elementos <<%d>>...\n", this->atributos->elementos_int);
             }
             if(error == 0){
                 // Copiar valores de this->atributos->array_uni_int a array_uni
                 for(i = 0; i < this->atributos->elementos_int; i++){
                     array_uni[i] = this->atributos->array_uni_int[i];
                 }
             }           
         }
         else{
             printf("El array uni int no tiene memoria asignada...\n");
         }
     }
     else{
         printf("El objeto this no tiene memoria asignada...\n");
     }
     return array_uni;
 }
 
 // Se define de una sola vez un array uni de tipo int (array_uni es de entrada)
 static void set_ListaInt(classLista *this, int *array_uni, int num_elementos){      
     if(this != NULL && array_uni != NULL){
       if(num_elementos >= 0){
         set_ElementosListaInt(this, num_elementos);
         int i;
         for(i = 0; i < get_TotalElementosListaInt(this); i++){
         this->atributos->array_uni_int[i] = array_uni[i];
         }    
       }
       else{
         printf("\n--> num_elementos <<%d>> debe ser mayor o igual a 0...\n", num_elementos);
       }
     }
     else{
         printf("El objeto this no tiene memoria asignada ó array_bidi == NULL...\n");
     }
 }
 
 // Se agrega un valor int al array uni de tipo int
 static void set_ValorListaInt(classLista *this, int indice, int valor){
     if(this != NULL){
         if(this->atributos->array_uni_int != NULL){
             if((indice >= 0 && indice < this->atributos->elementos_int)){
                 this->atributos->array_uni_int[indice] = valor;
             }
             else{   
               printf("Error en el indice --> <<%d>>\n", indice);                
             }
         }
         else{ 
             printf("El array uni int no tiene memoria asignada...\n");
         }
     }
     else{
         printf("El objeto this no tiene memoria asignada...\n");
     }
 }
 
 // Se obtiene un valor int del array uni de tipo int 
 static int get_ValorListaInt(classLista *this, int indice){
     int valor = -1;
     if(this != NULL){
         if(this->atributos->array_uni_int != NULL){
             if((indice >= 0 && indice < this->atributos->elementos_int)){
                 valor = this->atributos->array_uni_int[indice];
             }
             else{
                 printf("Error en el indice --> <<%d>>, se retorna -1...\n", indice);
             }
         }
         else{ 
             printf("El array uni int no tiene memoria asignada...\n");
         }
     }
     else{
         printf("El objeto this no tiene memoria asignada...\n");
     }
     return valor;
 }
 
 // Se libera la memoria de un array uni de tipo int     
 static void free_ListaInt(classLista *this){
     if(this != NULL){
         if(this->atributos->array_uni_int != NULL){ // Tiene memoria ya asignada    
             free(this->atributos->array_uni_int);
             this->atributos->array_uni_int = NULL;
             this->atributos->elementos_int = 1;
             printf("\n\tSe libero de manera correcta la memoria del array_uni_int...\n");
         }
         else{
             printf("El array uni int no tiene memoria asignada...\n");
         }
     }
     else{
         printf("El objeto this no tiene memoria asignada...\n");
     }
 }
 
 // Imprimir el array uni de tipo int
 static void print_ListaInt(classLista *this){
     if(this != NULL){
         if(this->atributos->array_uni_int != NULL){
             int i;
             for(i = 0; i < this->atributos->elementos_int; i++){
                 printf(" %d ", this->atributos->array_uni_int[i]);
             }
             printf("\n");
         }
         else{
             printf("El array uni int no tiene memoria asignada...\n");
         }
     }
     else{
         printf("El objeto this no tiene memoria asignada...\n");
     }
 }
 
 // Obtener el numero de elementos del array uni int
 static int get_TotalElementosListaInt(classLista *this){
     int elementos = -1;
     if(this != NULL){
         if(this->atributos->array_uni_int != NULL){
             elementos = this->atributos->elementos_int;
         }
         else{
             printf("this->atributos->elementos_int no tiene memoria asignada...\n");
         }
     }
     else{
         printf("El objeto this no tiene memoria asignada...\n");
     }
     return elementos;
 }


/*********************************************************************
 **                                                                  **
 ** INSERCIÒN DE NODOS E IMPLEMENTACIÒN                              **
 **                                                                  **
 *********************************************************************/

/*struct StructNodo{
	int dato;
	//struct StructNodo *siguiente; 
}
	

typedef struct StructNodo Nodo;
nodo*crearLista (nodo*lista);
nodo*insertarNodoInicio(int valor,nodo * lista);
nodo*insertarNodoFinal(int valor,nodo* lista);
nodo *retirarInicio(nodo *lista);
nodo *retirarFinal(nodo *lista);


//Implementaciòn de las FUnciones
nodo *crearLista(nodo *lista){
	return lista=NULL;
}
nodo *insertarNodoInicio(int valor,nodo*lista){
	nodo* nodoNuevo;
	nodoNuevo=(nodo*)malloc (sizeof (nodo));
	if(nodoNuevo !=NULL){
		nodoNuevo->dato=valor;
		nodoNuevo->siguiente=lista;
		lista=nodoNuevo;
}

return lista;

}

nodo*insertarNodoFinal(int valor,nodo*lista){
	nodo* nodoNuevo, nodoAuxiliar;
	nodoNuevo=(nodo*)malloc (sizeof (nodo));
	if(nodoNuevo !=NULL){
		nodoNuevo ->dato=valor;
		nodoNuevo -> siguiente=NULL;
	if(lista==NULL) lista=nodoNuevo;
		else {
			nodoAuxiliar=lista;
		while(nodoAuxiliar -> siguiente !=NULL)
			nodoAuxiliar=nodoAuxiliar->siguiente;

			nodoAuxiliar->siguiente=nodoNuevo;
}
		
}
	return lista;

}

//Retirar nodos de la lista

nodo*retirarInicio(nodo*lista){
	nodo*nodoAuxiliar;
	int dato;

if(lista==NULL)
printf("NO hay elementos el las lista");
else{

	nodoAuxiliar=lista;
	dato=nodoAuxiliar->dato;

	lista=nodoAuxiliar->siguiente;
	printf("Se ha retirado el elemento %d\n",dato);

//Liberar el espacio de memoria
	free(nodoAuxiliar);

}
	return lista;
}


//Retirar un nodo al final de la lista

nodo*retirarFinal(nodo*lista){
	nodo*nodoAuxiliar,nodoAnterior;
	int dato;

if(lista==NULL)
printf("NO hay elementos el las lista");
else{
	nodoAuxiliar=lista;
	while (nodoAuxiliar->siguiente !=NULL){
		nodoAnterior=nodoAuxiliar;
		nodoAuxiliar=nodoAuxiliar->siguiente;
}
	dato=nodoAuxiliar->dato;

	this->siguiente=NULL;
	printf("Se ha retirado el elemento %d\n",dato);

//Liberar el espacio de memoria
	free(nodoAuxiliar);

}
	return lista;
}
*/
