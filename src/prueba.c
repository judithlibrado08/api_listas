/*
* Este programa sirve para verificar el funcionamiento del API "arreglo_lista.so"
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

//#include "arreglo_lista.h"

struct structNodo{
	int dato;
	struct structNodo*siguiente;
}


//typedef struct structNodo nodo;
nodo*crearLista (nodo*lista);
nodo*insertarNodoInicio(int valor,nodo * lista);
nodo*insertarNodoFinal(int valor,nodo * lista);

//Funciòn para mostrar los elementos que hay en la lista
void ImprimirLista (nodo,lista);

int main(){
	nodo*lista;
	lista=crearLista(lista);
	lista=insertarNodoFinal(5,lista);
	imprimirLista(lista);

	return 0;
}

//Implementaciòn de las FUnciones
nodo *crearLista(nodo *lista){
	return lista=NULL;
}
nodo*insertarNodoInicio(int valor,nodo*lista){
	nodo* nodoNuevo;
	nodoNuevo=(nodo *) malloc (sizeof (nodo));
	if(nodoNuevo !=NULL){
		nodoNuevo ->dato=valor;
		nodoNuevo -> siguiente=lista;
		lista=nodoNuevo;
}
	return lista;

}

nodo*insertarNodoFinal(int valor,nodo*lista){
	nodo* nodoNuevo, nodoAuxiliar;
	nodoNuevo=(nodo *) malloc (sizeof (nodo));
	if(nodoNuevo !=NULL){
		nodoNuevo ->dato=valor;
		nodoNuevo -> siguiente=NULL;
	if(lista==NULL) 
	lista=nodoNuevo;
		else {
			nodoAuxiliar=lista;
		while(nodoAuxiliar -> siguiente !=NULL)
			nodoAuxiliar=nodoAuxiliar->siguiente;

			nodoAuxiliar->siguiente=nodoNuevo;
}
		
}
	return lista;

}

void imprimirLista(nodo *lista){
	nodo *nodoAuxiliar;
	nodoAuxiliar=lista;
	printf("INICIO ->");

	while(nodoAuxiliar!=NULL){
		printf("%d -> ",nodoAuxiliar ->dato);
		nodoAuxiliar=nodoAuxiliar->siguiente;

}
	printf("NULL\n");
}
